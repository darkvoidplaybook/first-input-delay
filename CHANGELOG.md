# Changelog

### 0.1.1 (2018-06-09)

- Fix erroneous values on certain browsers (#5)

### 0.1.0 (2018-05-08)

- Initial public release
